# coding=UTF-8
import string

from flask import Flask, request, json
import argparse
import requests
import sys
import re
import urllib

reload(sys)
sys.setdefaultencoding('utf-8')

app = Flask(__name__)

mentioned_list = ''


def getComment(type, params):
    content = ''
    global mentioned_list
    # 当前操作为合并请求时
    if type == 'merge_request':
        # 仓库项目主页地址path_with_namespace
        repository_nameUrl = params['project']['homepage']
        # 仓库项目名称
        repository_name = params['project']['path_with_namespace']
        # 项目信息标签
        object_attributes = params['object_attributes']
        # 当前合并地址
        merge_url = object_attributes['url']
        # 任务状态
        state = object_attributes['state']
        # 执行方向
        action = object_attributes.get('action')
        pattern = 'https?://(?:[-\w.]|(?:%[\da-fA-F]{2}))+(\:\d+)?'
        rootUrl = re.search(pattern, repository_nameUrl).group()
        # 打开状态字典
        openedStateTypeDist = {'open': '创建了', 'close': '关闭了', 'reopen': '重新打开了', 'approved': '审核', 'merge': '接受'}
        user = params['user']
        # 获取当前操作人员
        user_name = user['name']
        # 获取当前操作人员
        userName = user['username']
        # 提交分支
        sourcrBranch = object_attributes.get('source_branch')
        # 目标分支
        targetBranch = object_attributes.get('target_branch')
        # 指派列表
        assigneesList = params.get('assignees')
        commitTitle = ''
        # commitTitle = object_attributes.get('last_commit')['title']
        # if commitTitle.find('!'):
        # print(commitTitle.find('!'))
        commitTitle = object_attributes.get('title')
        if '合并' in commitTitle:
            commitTitle = commitTitle
        else:   
            commitTitle = commitTitle + '( ' + sourcrBranch + ' 合并到 ' + targetBranch + ' )'
        if action == 'open':
            if assigneesList is None:
                content = '[' + str(user_name) + '](' + str(rootUrl) + '/' + str(userName) + ') 在 [' + str(
                    repository_name) + '](' + str(repository_nameUrl) + ') 创建了 Pull Request [' + str(
                    commitTitle) + '](' + str(merge_url) + ')' + '\n'
            else:
                # 指派人循环
                assigneesList = ''
                assigneesPeople = ''
                for object in params['assignees']:
                    assigneesList = assigneesList + '[' + object['name'] + '](' + str(rootUrl) + '/' + object[
                        'username'] + ')、'
                    assigneesPeople = assigneesPeople + object['name'] + '、'
                assigneesList = assigneesList.rstrip('、')
                assigneesPeople = assigneesPeople.rstrip('、')
                content = '[' + str(user_name) + '](' + str(rootUrl) + '/' + str(
                    userName) + ') 指派了 ' + assigneesList + ' 审查 [' + str(
                    repository_name) + '](' + str(repository_nameUrl) + ') Pull Request [' + str(
                    commitTitle) + '](' + str(merge_url) + ')' + '\n'
                # mentioned_list = assigneesPeople
                mentioned_list = '18397412168'
        if action == 'reopen':
            content = '[' + str(user_name) + '](' + str(rootUrl) + '/' + str(userName) + ') 在 [' + str(
                repository_name) + '](' + str(repository_nameUrl) + ') 重新打开了 Pull Request [' + str(
                commitTitle) + '](' + str(merge_url) + ')' + '\n'
        if action == 'close':
            content = '[' + str(user_name) + '](' + str(rootUrl) + '/' + str(userName) + ') 关闭了 [' + str(
                repository_name) + '](' + str(repository_nameUrl) + ') 的 Pull Request [' + str(
                commitTitle) + '](' + str(merge_url) + ')' + '\n'
        if action == 'approved':
            content = '[' + str(user_name) + '](' + str(rootUrl) + '/' + str(userName) + ') 审核通过了 [' + str(
                repository_name) + '](' + str(repository_nameUrl) + ') 的 Pull Request [' + str(
                commitTitle) + '](' + str(merge_url) + ')' + '\n'
        if action == 'merge':
            content = '[' + str(user_name) + '](' + str(rootUrl) + '/' + str(userName) + ') 接受了 [' + str(
                repository_name) + '](' + str(repository_nameUrl) + ') 的 Pull Request [' + str(
                commitTitle) + '](' + str(merge_url) + ')' + '\n'
    elif type == 'push':
        print('')
    if commitTitle is None:
        print('标题为空不发送')
        return '';
    return content


@app.route('/WxWebhook', methods=['POST'])
def webhook():
    # 请求参数
    params = request.json
    keys = request.args.get('keys')
    if keys is None:
        return '请求参数不能为空'
    # 用户名
    user_name = params.get('user_name')
    user = params.get('user')
    if user_name is None:
        user_name = user.get('name')
        if len(user_name) == 0:
            user_name = ''
            # 提交任务状态
    object_attributes = params.get('object_attributes')
    # 提交类型
    object_kind = params.get('object_kind')
    # 总commits数
    # total_commits_count = str(params['total_commits_count'])
    # 项目名称
    repository_name = params['project']['name']
    # 分支
    # ref = params['ref']
    # 发送内容
    content = getComment(object_kind, params)
    global mentioned_list
    # 引用形式列出提交的id
    # for object in params['commits']:
    # content = content + '&gt; ['+ object['id'][0:8] +']('+ object['url'] +')' + ': ' + object['title'] + '\n'
    body = ''
    if mentioned_list is None:
        body = {
            "msgtype": "markdown",
            "markdown": {
                "content": content
            }
        }
    else:
        body = {
            "msgtype": "markdown",
            "markdown": {
                "content": content,
                "mentioned_mobile_list": ["陈滕"]
            }
        }
    print(body)
    if content is not None:
        requests.request('POST', keys, headers={'Content-Type': 'application/json'}, json=body)
    return 'OK'


if __name__ == '__main__':
    # 获取ArgumentParser对象
    # parser = argparse.ArgumentParser()
    # 添加参数
    # parser.add_argument('--wechatUrl', type=str)
    # args是一个命名空间
    # url = args = parser.parse_args().wechatUrl
    # if url is not None:
    # 可以接收webhook的地址和端口，可以指定GitLab地址 自定义端口
    app.run('0.0.0.0', 9081)
